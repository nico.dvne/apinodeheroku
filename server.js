import express from 'express'
import mongoose from 'mongoose'
import routes from './routes/routes.js'
import privateRoute from './routes/privateRoutes.js'
import passport from 'passport'

import './auth/auth.js';

import dotenv from 'dotenv'
dotenv.config()


const PORT = 8080;

const app = express()

app.use(express.json())

mongoose.connect(process.env.MONGODB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
})

app.use(
  '/private',
  passport.authenticate('jwt', {session: false}),
  privateRoute);

app.use(routes)

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`)
});
