import express from 'express'
import passport from '../auth/auth.js';

import pkg from 'jsonwebtoken';
const {sign} = pkg;

import { catchErrors } from '../helpers.js'
import {
  getRooms,
  getRoom
} from '../controllers/roomControllers.js'

// Path avec ES module
import path, { dirname } from 'path'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

const router = express.Router()

router.get('/api/rooms', catchErrors(getRooms))

router.get('/api/rooms/:id', catchErrors(getRoom))


// Authentification 


//passport.authenticate('signup') va chercher le signup de passport ( déclaré dans auth.js )
router.post(
  '/signup',
  passport.authenticate('signup', {'session': false}), 
    async (req, res, next) => { 
      res.json(
        {
          message: 'Sign up ok',
          user: req.user
        }
    )}
)

router.post(
  '/login',
  (req, res, next) => {
    passport.authenticate('login', async (err, user) => {
      try {
        if (err) {
          const error = new Error('Une erreur est survenue');

          return next(error);
        }

        console.log(req);

        if (!user) {
          return next(new Error("Email ou mdp incorrect"));
        }

        req.login(user, {session: false}, async (error) => {
          if (error) {
            return next(error);
          }

          const body = {id: user._id, email: user.email};
          const token = sign({ user:body }, 'motdepasse');

          res.json({token});
        })

      } catch {
        return next(error);
      }
    })(req, res, next)
})

export default router
