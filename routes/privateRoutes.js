import express from 'express'
import { catchErrors } from '../helpers.js'

const router = express.Router()
import {
    addRoom,
    deleteRoom,
    updateRoom
  } from '../controllers/roomControllers.js'

router.post('/api/rooms', catchErrors(addRoom))

router.patch('/api/rooms/:id', catchErrors(updateRoom))

router.delete('/api/rooms/:id', catchErrors(deleteRoom))

router.get('/secret', (req, res) => {
    res.json({
        message: 'Tu es co au secret gros',
        user: req.user,
        token: req.query.token
    })
})

export default router;